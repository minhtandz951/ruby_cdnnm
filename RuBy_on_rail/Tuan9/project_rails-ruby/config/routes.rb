Rails.application.routes.draw do
  resources :books
  root "pages#home"
  resources :friends
  resources :books

  get 'about', to: 'pages#about'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
  get 'pages/home'
end

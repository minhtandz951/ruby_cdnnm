require "test_helper"

class BooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @book = books(:one)
  end

  test "should get index" do
    get books_url
    assert_response :success
  end

  test "should get new" do
    get new_book_url
    assert_response :success
  end

  test "should create book" do
    assert_difference("Book.count") do
      post books_url, params: { book: { ghichu_236: @book.ghichu_236, hoten_236: @book.hoten_236, lop_236: @book.lop_236, mahs_236: @book.mahs_236, manganh_236: @book.manganh_236, ngaymuon_236: @book.ngaymuon_236, ngaytra_236: @book.ngaytra_236, soBM_236: @book.soBM_236, tensach_236: @book.tensach_236, tentacgia_236: @book.tentacgia_236 } }
    end

    assert_redirected_to book_url(Book.last)
  end

  test "should show book" do
    get book_url(@book)
    assert_response :success
  end

  test "should get edit" do
    get edit_book_url(@book)
    assert_response :success
  end

  test "should update book" do
    patch book_url(@book), params: { book: { ghichu_236: @book.ghichu_236, hoten_236: @book.hoten_236, lop_236: @book.lop_236, mahs_236: @book.mahs_236, manganh_236: @book.manganh_236, ngaymuon_236: @book.ngaymuon_236, ngaytra_236: @book.ngaytra_236, soBM_236: @book.soBM_236, tensach_236: @book.tensach_236, tentacgia_236: @book.tentacgia_236 } }
    assert_redirected_to book_url(@book)
  end

  test "should destroy book" do
    assert_difference("Book.count", -1) do
      delete book_url(@book)
    end

    assert_redirected_to books_url
  end
end

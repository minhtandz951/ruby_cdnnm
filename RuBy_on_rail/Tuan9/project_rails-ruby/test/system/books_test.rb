require "application_system_test_case"

class BooksTest < ApplicationSystemTestCase
  setup do
    @book = books(:one)
  end

  test "visiting the index" do
    visit books_url
    assert_selector "h1", text: "Books"
  end

  test "should create book" do
    visit books_url
    click_on "New book"

    fill_in "Ghichu 236", with: @book.ghichu_236
    fill_in "Hoten 236", with: @book.hoten_236
    fill_in "Lop 236", with: @book.lop_236
    fill_in "Mahs 236", with: @book.mahs_236
    fill_in "Manganh 236", with: @book.manganh_236
    fill_in "Ngaymuon 236", with: @book.ngaymuon_236
    fill_in "Ngaytra 236", with: @book.ngaytra_236
    fill_in "Sobm 236", with: @book.soBM_236
    fill_in "Tensach 236", with: @book.tensach_236
    fill_in "Tentacgia 236", with: @book.tentacgia_236
    click_on "Create Book"

    assert_text "Book was successfully created"
    click_on "Back"
  end

  test "should update Book" do
    visit book_url(@book)
    click_on "Edit this book", match: :first

    fill_in "Ghichu 236", with: @book.ghichu_236
    fill_in "Hoten 236", with: @book.hoten_236
    fill_in "Lop 236", with: @book.lop_236
    fill_in "Mahs 236", with: @book.mahs_236
    fill_in "Manganh 236", with: @book.manganh_236
    fill_in "Ngaymuon 236", with: @book.ngaymuon_236
    fill_in "Ngaytra 236", with: @book.ngaytra_236
    fill_in "Sobm 236", with: @book.soBM_236
    fill_in "Tensach 236", with: @book.tensach_236
    fill_in "Tentacgia 236", with: @book.tentacgia_236
    click_on "Update Book"

    assert_text "Book was successfully updated"
    click_on "Back"
  end

  test "should destroy Book" do
    visit book_url(@book)
    click_on "Destroy this book", match: :first

    assert_text "Book was successfully destroyed"
  end
end

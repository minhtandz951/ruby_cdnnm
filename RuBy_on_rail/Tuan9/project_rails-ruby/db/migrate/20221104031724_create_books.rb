class CreateBooks < ActiveRecord::Migration[7.0]
  def change
    create_table :books do |t|
      t.string :mahs_236
      t.string :hoten_236
      t.string :lop_236
      t.string :manganh_236
      t.string :tensach_236
      t.string :tentacgia_236
      t.string :soBM_236
      t.date :ngaymuon_236
      t.date :ngaytra_236
      t.text :ghichu_236

      t.timestamps
    end
  end
end

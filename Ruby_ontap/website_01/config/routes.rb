Rails.application.routes.draw do
  devise_for :users
  root to: "vd#index"
  get 'profile', to: 'vd#profile', as: :user_profile
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
